import Vue from 'vue'


// import and register fontawesome
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('icon', Icon)


// include vue-custom-element plugin to Vue
import VueCustomElement from 'vue-custom-element'
Vue.use(VueCustomElement)

// import and register your component(s)
import QuestionAndAnswerComponent from './components/QuestionAndAnswerComponent'
new Vue.customElement('question-answer', QuestionAndAnswerComponent)


//import QuestionAndAnswerLargeComponent from './components/QuestionAndAnswerLargeComponent'
//new Vue.customElement('qna-l', QuestionAndAnswerLargeComponent)

Vue.component('modal', {
  template: '#modal-template'
})
